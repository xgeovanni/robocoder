# Title
# Description
# Bobby Clarke

#Imports
import pygame
import sys
from pygame.locals import *
pygame.init()

# Imports for RoboCode
import time

#Linking
from robot import Robot

clock = pygame.time.Clock()
screen = pygame.display.set_mode((640, 480))

constants = {"midScreen" : [i / 2 for i in screen.get_size()],
             "white" : (255, 255, 255),
             "black" : (0, 0, 0),
             }

class Mode():
    def __init__(self):
        self.settings = {"framerate" : 30,
                         "bgColour" : (255, 255, 255),
                         "robotColour" : (0, 0, 0),
                         "robotStartPos" : constants["midScreen"],
                         "robotSpeed" : 3}
        
    def __getitem__(self, key):
        return self.settings[key]
    def __setitem__(self, key, new):
        self.settings[key] = new
    
    def get(self, key):
        return self[key]
    def _set(self, key, new):
        self[key] = new

def createMode():
    global mode
    mode = Mode()        
    
def createRobot():
    global robot 
    robot = Robot(mode.get("robotColour"),
                  mode.get("robotStartPos"),
                  mode.get("robotSpeed"))

def main(codeQueue):
    codeQueuePos = 0
    
    while True:
        clock.tick(mode.get("framerate"))
        screen.fill(mode.get("bgColour"))
        
        robot.draw()
        robot.move()
        
        if not robot.checkMovesLeft():
            if codeQueuePos in range(len(codeQueue)):
                exec(codeQueue[codeQueuePos])
                codeQueuePos += 1
                

        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
                


