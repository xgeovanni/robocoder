import gui
import pygame

def repColour(img, baseColour, newColour):
    pximg = pygame.PixelArray(img)
    pximg.replace(baseColour, newColour)
    return pximg.make_surface().convert_alpha()

class Robot():
    def __init__(self, colour, startpos, speed):
        white = (255, 255, 255)
        
        img = pygame.image.load("Images/baserobot.png")
        self.img = repColour(img, white, colour)
        
        self.x = startpos[0]
        self.y = startpos[1]
        
        self.speed = speed
        
        self.upmoves = 0
        self.downmoves = 0
        self.leftmoves = 0
        self.rightmoves = 0
        
    def draw(self):
        gui.screen.blit(self.img, (self.x, self.y))
        
    def move(self):
        if self.upmoves > 0:
            self.y -= self.speed
            self.upmoves -= 1
        elif self.downmoves > 0:
            self.y += self.speed
            self.downmoves -= 1
        if self.leftmoves > 0:
            self.x -= self.speed
            self.leftmoves -= 1
        elif self.rightmoves > 0:
            self.x += self.speed
            self.rightmoves -= 1
            
    def addUpMoves(self, num):
        self.upmoves += num
    def addDownMoves(self, num):
        self.downmoves += num
    def addLeftMoves(self, num):
        self.leftmoves += num
    def addRightMoves(self, num):
        self.rightmoves += num

    def checkMovesLeft(self):
        if (self.upmoves or self.downmoves or 
            self.leftmoves or self.rightmoves):
            return True
        else:
            return False
        