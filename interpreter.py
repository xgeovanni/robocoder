# Interpreter for bot code

import gui
import time

constants = gui.constants

def stringTuple(totuple):
    """Takes a string and creates a tuple from it,
       elements are seperated by spaces"""

    newtuple = []
    newelement = ""

    for char in str(totuple):
        if char != " ":
            newelement += char
        else:
            if newelement:
                newtuple.append(newelement)
                newelement = ""

    if newelement:
        newtuple.append(newelement)

    return tuple(newtuple)

class Interpreter():
    def __init__(self, bot, addCommands = []):
        self.bot = open(bot, "r")
        validCommands = ["up", "down", "left", "right", "sleep"]
        validCommands.extend(addCommands)
        
        self.validCommands = tuple(validCommands)

    def interpret(self):
        codeQueue = []
        moveCommands = ("up", "down", "left", "right")
        
        for line in self.bot.readlines():
            line = line.strip("\n").lower()
            
            commands = stringTuple(line)
            if not line or "#" in line:
                pass # Blank line
            
            elif commands[0] not in self.validCommands:
                print("Error in line:", line)
                
            else:
                for command in commands:
                    if command in constants:
                        index = commands.index(command)
                        commands.pop(index)
                        commands.insert(index, constants[command])
                        
                if len(commands) >= 4:
                    # Diagonal Movement
                    
                    if (commands[0] in moveCommands and
                        commands[2] in moveCommands):
                        codeQueue.append("""robot.add{0}Moves({1})
robot.add{2}Moves({3})""".format(commands[0].title(),
                                 commands[1],
                                 commands[2].title(),
                                 commands[3]))
                
                elif commands[0] in moveCommands:
                    # Vertical and horizontal moves
                    codeQueue.append("robot.add{0}Moves({1})"
                                     .format(commands[0].title(),
                                             commands[1]))
                # Other Commands           
                elif commands[0] == "sleep":
                    codeQueue.append("time.sleep({0})"
                                     .format(1 / 1000 * int(commands[1])))
                    
        return codeQueue
                
    def closeBot(self):
        self.bot.close()
            
def main():
    interpreter = Interpreter("Code/bot.txt")
    codeQueue = interpreter.interpret()
    interpreter.closeBot()
    
    return codeQueue
  
    
        
        