# Linking
import gui

constants = gui.constants

def findnew(line):
    index = 0
    
    for char in line:
        index += 1
        
        if char == "=":
            break
        
    return line[index :], line[:index - 1]

def sortSettings():
    settings = open("Code/settings.txt", "r")
    
    for line in settings.readlines():
        line = line.strip(" ").strip("\n")
        
        if "=" in line and "#" not in line:
            new, var = findnew(line)
            new = new.strip(" ")
            var = var.strip(" ")
            
            if "(" in new and ")" in new:
                new = eval(new)
            elif new in constants:
                new = constants[new]
            else:
                new = int(new)
                
            gui.mode._set(var, new)
            
    settings.close()
            
def main():
    sortSettings()

