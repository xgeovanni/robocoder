# Robot Coder
# alpha

# LOGO like game / educational 
# tool allowing users to control a robot through "RoboCode"

# GNU General Public License 3.0

# Imports
import os
#    #    #    #    #

# Linking
import gui
from settingHandler import main as settingHandler
from interpreter import main as interpret
#    #    #    #    #

def checkInit():
    """Check if the required files have been created"""
    
    if os.path.exists("Code"):
        return True
    else:
        from __init__ import main as __init__
        __init__()
        
        return False
    
def main():
    checkInit()
    gui.createMode()
    settingHandler()
    gui.createRobot()
    codeQueue = interpret()
    gui.main(codeQueue)
    
if __name__ == "__main__":
    main()
